﻿using DatabaseLayer;
using LibraryManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Controllers
{
    public class PurchaseController : Controller
    {
        private OnlineLibraryMgtSystemDbEntities db = new OnlineLibraryMgtSystemDbEntities();

        public ActionResult NewPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            double total = 0;
            var tempPur = db.PurTemDetailsTables.ToList();
            foreach(var item in tempPur)
            {
                total += (item.Qty * item.UnitPrice);
            }
            ViewBag.TotalAmount = total;
            return View(tempPur);
        }

        [HttpPost]
        public ActionResult AddItem(int BID, int Qty, int Price)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["UserID"]));
            var find = db.PurTemDetailsTables.Where(i => i.BookID == BID).FirstOrDefault();
            if (find == null)
            {
                if (BID > 0 && Qty > 0 && Price > 0)
                {
                    var newItem = new PurTemDetailsTable()
                    {
                        BookID = BID,
                        Qty = Qty,
                        UnitPrice = Price
                    };
                    db.PurTemDetailsTables.Add(newItem);
                    db.SaveChanges();
                    ViewBag.Message = "Book Add Successs";
                }
            }
            else
            {
                ViewBag.Message = "Already Exist! Plz Check";
            }
            return RedirectToAction("NewPurchase");
        }

        public ActionResult DeleteConfirm(int id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var book = db.PurTemDetailsTables.Find(id);
            if (book != null)
            {
                db.Entry(book).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
                ViewBag.Message = "Delete Success";
                return RedirectToAction("NewPurchase");
            }
            ViewBag.Message = "Some Unexptected issue is occure, please contact to concern person!";
            return RedirectToAction("NewPurchase");
        }

        [HttpGet]
        public ActionResult GetBooks()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            List<BookMV> list = new List<BookMV>();
            var bookLst = db.BookTables.ToList();
            foreach (var item in bookLst)
            {
                list.Add(new BookMV { BookName = item.BookName, BookID = item.BookID });
            }

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CancelPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var list = db.PurTemDetailsTables.ToList();
            bool cancelStatus = false;
            foreach (var item in list)
            {
                db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                int noOfRecord = db.SaveChanges();
                if (cancelStatus == false)
                {
                    if (noOfRecord > 0)
                    {
                        cancelStatus = true;
                    }
                }
            }
            if (cancelStatus == true)
            {
                ViewBag.Message = "Purchase is Canceled";
                return RedirectToAction("NewPurchase");
            }
            ViewBag.Message = "Some Unexptected issue is occure, please contact to concern person!";
            return RedirectToAction("NewPurchase");
        }

        public ActionResult SelectSupplier()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var purchaseLst = db.PurTemDetailsTables.ToList();
            if (purchaseLst.Count == 0)
            {
                ViewBag.Message = "Purchase cart empty";
                return RedirectToAction("NewPurchase");
            }
            var suppliers = db.SupplierTables.ToList();
            return View(suppliers);
        }

        [HttpPost]
        public ActionResult PurchaseConfirm(FormCollection collection)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["UserID"]));
            int supplierId = 0;
            string[] keys = collection.AllKeys;
            foreach (var name in keys)
            {
                if (name.Contains("name"))
                {
                    string idName = name;
                    string[] valIds = idName.Split(' ');
                    supplierId = Convert.ToInt32(valIds[1]);
                }
            }
            var purchaseTemps = db.PurTemDetailsTables.ToList();
            double total = 0;
            foreach (var item in purchaseTemps)
            {
                total += (item.Qty * item.UnitPrice);
            }
            if (total == 0)
            {
                ViewBag.Message = "Purchase cart empty";
                return View("NewPurchase");
            }

            var purchaseHeader = new PurchaseTable();
            purchaseHeader.SupplierID = supplierId;
            purchaseHeader.PurchaseDate = DateTime.Now;
            purchaseHeader.PurchaseAmount = total;
            purchaseHeader.UserID = userid;
            db.PurchaseTables.Add(purchaseHeader);
            db.SaveChanges();

            foreach (var item in purchaseTemps)
            {
                var itemPurchase = new PurchaseDetailTable()
                {
                    BookID = item.BookID,
                    PurchaseID = purchaseHeader.PurchaseID,
                    Qty = item.Qty,
                    UnitPrice = item.UnitPrice
                };
                db.PurchaseDetailTables.Add(itemPurchase);
                db.SaveChanges();

                var updateBookStock = db.BookTables.Find(item.BookID);
                updateBookStock.TotalCopies = updateBookStock.TotalCopies + item.Qty;
                updateBookStock.Price = item.UnitPrice;
                db.Entry(updateBookStock).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            db.PurTemDetailsTables.ToList().ForEach(el => {
                db.Entry(el).State = System.Data.Entity.EntityState.Deleted;
            });
            db.SaveChanges();
            ViewBag.Message = "Purchase Confirm success";
            return RedirectToAction("AllPurchase");
        }

        public ActionResult AllPurchase()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            var lst = db.PurchaseTables.ToList();
            
            return View(lst);
        }

        public ActionResult PurchaseDetailsView(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var purchaseDatail = db.PurchaseDetailTables.Where(item => item.PurchaseID == id);
            if (purchaseDatail == null)
            {
                return HttpNotFound();
            }
            return View(purchaseDatail);
        }
    }
}
