﻿using DatabaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Controllers
{
    public class ReserveBookController : Controller
    {
        private OnlineLibraryMgtSystemDbEntities db = new OnlineLibraryMgtSystemDbEntities();

        // GET: ReserveBook
        public static string Message { get; set; }
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = Message;
            Message = string.Empty;
            var books = db.BookTables.ToList();
            return View(books);
        }

        public ActionResult ReserveBook(int? id)
        {
            var book = db.BookTables.Find(id);
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["UserID"]));
            int employeeid = Convert.ToInt32(Convert.ToString(Session["EmployeeID"]));
            var issueBookTable = new IssueBookTable()
            {
                BookID = book.BookID,
                Description = "Reserve Request!",
                EmployeeID = employeeid,
                IssueCopies = 1,
                IssueDate = DateTime.Now,
                ReturnDate = DateTime.Now.AddDays(2),
                Status = false,
                ReserveNoOfCopies = true,
                UserID = userid
            };
            issueBookTable.UserID = userid;
            if (ModelState.IsValid)
            {
                var find = db.IssueBookTables.Where(i => i.ReturnDate >= DateTime.Now && i.BookID == issueBookTable.BookID && (i.Status == true || i.ReserveNoOfCopies == true)).ToList();
                int issueCountBook = 0;
                foreach (var item in find)
                {
                    issueCountBook += item.IssueCopies;
                }
                var stockBook = db.BookTables.Where(b => b.BookID == issueBookTable.BookID).FirstOrDefault();
                if ((issueCountBook == stockBook.TotalCopies) || (issueCountBook + issueBookTable.IssueCopies > stockBook.TotalCopies))
                {
                    Message = "Stock is Empty";
                    return RedirectToAction("Index");
                }
                db.IssueBookTables.Add(issueBookTable);
                db.SaveChanges();
                Message = "Book Issue Successfully!";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}