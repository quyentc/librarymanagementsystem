﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DatabaseLayer;

namespace LibraryManagementSystem.Controllers
{
    public class IssueBookTablesController : Controller
    {
        private OnlineLibraryMgtSystemDbEntities db = new OnlineLibraryMgtSystemDbEntities();

        // GET: IssueBookTables
        public ActionResult IssueBooks()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            
            var issueBookTables = db.IssueBookTables.Include(i => i.BookTable).Include(i => i.EmployeeTable).Include(i => i.UserTable).Where(b => b.Status == true && b.ReserveNoOfCopies == false);
            return View(issueBookTables.ToList());
        }

        public ActionResult ReserveBooks()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }

            var issueBookTables = db.IssueBookTables.Include(i => i.BookTable).Include(i => i.EmployeeTable).Include(i => i.UserTable).Where(b => b.Status == false && b.ReserveNoOfCopies == true && b.ReturnDate > DateTime.Now);
            return View(issueBookTables.ToList());
        }

        public ActionResult ReturnPendingBooks()
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            //&& (b.ReturnDate - DateTime.Now).Days <= 3
            List<IssueBookTable> lst = new List<IssueBookTable>();
            var issueBookTables = db.IssueBookTables.Where(b => b.Status == true || b.ReserveNoOfCopies == true).ToList();
            foreach (var item in issueBookTables)
            {
                var returnDate = item.ReturnDate;
                int noOfDay = (returnDate - DateTime.Now).Days;
                if (noOfDay <= 3)
                {
                    lst.Add(new IssueBookTable
                    {
                        BookID = item.BookID,
                        BookTable = item.BookTable,
                        Description = item.Description,
                        EmployeeID = item.EmployeeID,
                        EmployeeTable = item.EmployeeTable,
                        IssueBookID = item.IssueBookID,
                        IssueCopies = item.IssueCopies,
                        IssueDate = item.IssueDate,
                        ReserveNoOfCopies = item.ReserveNoOfCopies,
                        ReturnDate = item.ReturnDate,
                        Status = item.Status,
                        UserID = item.UserID,
                        UserTable = item.UserTable
                    });
                }
            }
            
            return View(lst.ToList());
        }

        // GET: IssueBookTables/Details/5
        public ActionResult Details(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IssueBookTable issueBookTable = db.IssueBookTables.Find(id);
            if (issueBookTable == null)
            {
                return HttpNotFound();
            }
            return View(issueBookTable);
        }

        // GET: IssueBookTables/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", "0");
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "FullName", "0");
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", "0");
            return View();
        }

        // POST: IssueBookTables/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IssueBookTable issueBookTable)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["UserID"]));
            issueBookTable.UserID = userid;
            if (ModelState.IsValid)
            {
                var find = db.IssueBookTables.Where(i => i.ReturnDate >= DateTime.Now && i.BookID == issueBookTable.BookID && (i.Status == true || i.ReserveNoOfCopies == true)).ToList();
                int issueCountBook = 0;
                foreach(var item in find)
                {
                    issueCountBook += item.IssueCopies;
                }
                var stockBook = db.BookTables.Where(b => b.BookID == issueBookTable.BookID).FirstOrDefault();
                if ((issueCountBook == stockBook.TotalCopies) || (issueCountBook + issueBookTable.IssueCopies > stockBook.TotalCopies))
                {
                    ViewBag.Message = "Stock is Empty";
                    return View(issueBookTable);
                }


                db.IssueBookTables.Add(issueBookTable);
                db.SaveChanges();
                ViewBag.Message = "Book Issue Success";
                return RedirectToAction("IssueBooks");
            }

            ViewBag.BookID = new SelectList(db.BookTables, "BookID", "BookTitle", issueBookTable.BookID);
            ViewBag.EmployeeID = new SelectList(db.EmployeeTables, "EmployeeID", "FullName", issueBookTable.EmployeeID);
            ViewBag.UserID = new SelectList(db.UserTables, "UserID", "UserName", issueBookTable.UserID);
            return View(issueBookTable);
        }

        public ActionResult ApproveRequest(int? id)
        {
            var request = db.IssueBookTables.Find(id);
            request.ReserveNoOfCopies = false;
            request.Status = true;
            request.Description = "Approve";
            db.Entry(request).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ReserveBooks");
        }

        
        public ActionResult ReturnBook(int? id)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            {
                return RedirectToAction("Login", "Home");
            }
            int userid = Convert.ToInt32(Convert.ToString(Session["UserID"]));

            var book = db.IssueBookTables.Find(id);
            int fine = 0;
            var returnDate = book.ReturnDate;
            int noOfDay = (DateTime.Now - returnDate).Days;
            if(book.Status == true && book.ReserveNoOfCopies == false)
            {
                if(noOfDay > 0)
                {
                    fine = 20 * noOfDay;
                }
                var returnbook = new BookReturnTable()
                {
                    BookID = book.BookID,
                    CurrentDate = DateTime.Now,
                    EmployeeID = book.EmployeeID,
                    IssueDate = book.IssueDate,
                    ReturnDate = book.ReturnDate,
                    UserID = userid
                };
                db.BookReturnTables.Add(returnbook);
                db.SaveChanges();
            }

            book.Status = false;
            book.ReserveNoOfCopies = false;
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();

            if(fine > 0)
            {
                var addfine = new BookFineTable()
                {
                    BookID = book.BookID,
                    EmployeeID = book.EmployeeID,
                    FineAmount = fine,
                    FineDate = DateTime.Now,
                    NoOfDays = noOfDay,
                    ReceiveAmount = 0,
                    UserID = userid
                };
                db.BookFineTables.Add(addfine);
                db.SaveChanges();
            }

            return RedirectToAction("IssueBooks");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
