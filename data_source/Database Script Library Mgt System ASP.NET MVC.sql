USE [master]
GO
/****** Object:  Database [OnlineLibraryMgtSystemDb]    Script Date: 20/12/2019 10:45:30 PM ******/
CREATE DATABASE [OnlineLibraryMgtSystemDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OnlineLibraryMgtSystemDb', FILENAME = N'D:\FYP Library Management System 20191002 Qurtuba Uni Pesh\Database\OnlineLibraryMgtSystemDb.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'OnlineLibraryMgtSystemDb_log', FILENAME = N'D:\FYP Library Management System 20191002 Qurtuba Uni Pesh\Database\OnlineLibraryMgtSystemDb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OnlineLibraryMgtSystemDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET  MULTI_USER 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET DELAYED_DURABILITY = DISABLED 
GO
USE [OnlineLibraryMgtSystemDb]
GO
/****** Object:  Table [dbo].[BookFineTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookFineTable](
	[BookFineID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[BookID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[FineDate] [date] NOT NULL,
	[FineAmount] [float] NOT NULL,
	[ReceiveAmount] [float] NULL,
	[NoOfDays] [int] NOT NULL,
 CONSTRAINT [PK_BookFineTable] PRIMARY KEY CLUSTERED 
(
	[BookFineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BookReturnTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookReturnTable](
	[BookReturnID] [int] IDENTITY(1,1) NOT NULL,
	[BookID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[IssueDate] [date] NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[CurrentDate] [date] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_BookReturnTable] PRIMARY KEY CLUSTERED 
(
	[BookReturnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BookTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookTable](
	[BookID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[BookTypeID] [int] NOT NULL,
	[BookTitle] [nvarchar](200) NOT NULL,
	[ShortDescription] [nvarchar](500) NOT NULL,
	[Author] [nvarchar](150) NOT NULL,
	[BookName] [nvarchar](200) NOT NULL,
	[Edition] [float] NOT NULL,
	[TotalCopies] [int] NOT NULL,
	[RegDate] [date] NOT NULL,
	[Price] [float] NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_BookTable] PRIMARY KEY CLUSTERED 
(
	[BookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BookTypeTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookTypeTable](
	[BookTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_BookTypeTable] PRIMARY KEY CLUSTERED 
(
	[BookTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DepartmentTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartmentTable](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_DepartmentTable] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DesignationTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DesignationTable](
	[DesignationID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[UserID] [int] NOT NULL,
	[Scale] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DesignationTable] PRIMARY KEY CLUSTERED 
(
	[DesignationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeTable](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FullName] [nvarchar](200) NOT NULL,
	[FatherName] [nvarchar](150) NOT NULL,
	[ContactNo] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[DesignationID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_EmployeeTable] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IssueBookTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IssueBookTable](
	[IssueBookID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[BookID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[IssueCopies] [int] NOT NULL,
	[IssueDate] [date] NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[Status] [bit] NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ReserveNoOfCopies] [bit] NOT NULL,
 CONSTRAINT [PK_IssueBookTable] PRIMARY KEY CLUSTERED 
(
	[IssueBookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseDetailTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseDetailTable](
	[PurchaseDetailID] [int] IDENTITY(1,1) NOT NULL,
	[BookID] [int] NOT NULL,
	[PurchaseID] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[UnitPrice] [float] NOT NULL,
 CONSTRAINT [PK_PurchaseDetailTable] PRIMARY KEY CLUSTERED 
(
	[PurchaseDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseTable](
	[PurchaseID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseDate] [date] NOT NULL,
	[UserID] [int] NOT NULL,
	[PurchaseAmount] [float] NOT NULL,
	[SupplierID] [int] NOT NULL,
 CONSTRAINT [PK_PurchaseTable] PRIMARY KEY CLUSTERED 
(
	[PurchaseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurTemDetailsTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurTemDetailsTable](
	[PurTemID] [int] IDENTITY(1,1) NOT NULL,
	[BookID] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[UnitPrice] [float] NOT NULL,
 CONSTRAINT [PK_PurTemDetailsTable] PRIMARY KEY CLUSTERED 
(
	[PurTemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SupplierTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierTable](
	[SupplierID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[ContactNo] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_SupplierTable] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTable](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeID] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_UserTable_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTypeTable]    Script Date: 20/12/2019 10:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTypeTable](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_UserTypeTable] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[BookFineTable] ON 

INSERT [dbo].[BookFineTable] ([BookFineID], [EmployeeID], [BookID], [UserID], [FineDate], [FineAmount], [ReceiveAmount], [NoOfDays]) VALUES (2, 1, 1, 1, CAST(N'2019-12-20' AS Date), 100, 100, 5)
SET IDENTITY_INSERT [dbo].[BookFineTable] OFF
SET IDENTITY_INSERT [dbo].[BookReturnTable] ON 

INSERT [dbo].[BookReturnTable] ([BookReturnID], [BookID], [EmployeeID], [IssueDate], [ReturnDate], [CurrentDate], [UserID]) VALUES (3, 1, 1, CAST(N'2019-12-10' AS Date), CAST(N'2019-12-15' AS Date), CAST(N'2019-12-20' AS Date), 1)
SET IDENTITY_INSERT [dbo].[BookReturnTable] OFF
SET IDENTITY_INSERT [dbo].[BookTable] ON 

INSERT [dbo].[BookTable] ([BookID], [UserID], [DepartmentID], [BookTypeID], [BookTitle], [ShortDescription], [Author], [BookName], [Edition], [TotalCopies], [RegDate], [Price], [Description]) VALUES (1, 1, 1, 1, N'Health And Food', N'Health And Food Book', N'Salman', N'Health And Food', 5, 8, CAST(N'2019-12-19' AS Date), 5000, N'Health And Food Description Add here')
INSERT [dbo].[BookTable] ([BookID], [UserID], [DepartmentID], [BookTypeID], [BookTitle], [ShortDescription], [Author], [BookName], [Edition], [TotalCopies], [RegDate], [Price], [Description]) VALUES (2, 1, 1, 1, N'Food Advantages', N'add here short description', N'Hamza', N'Food Advantages', 5, 4, CAST(N'2019-12-20' AS Date), 4500, N'Add here Book Description')
SET IDENTITY_INSERT [dbo].[BookTable] OFF
SET IDENTITY_INSERT [dbo].[BookTypeTable] ON 

INSERT [dbo].[BookTypeTable] ([BookTypeID], [Name], [UserID]) VALUES (1, N'General Knowledge ', 1)
SET IDENTITY_INSERT [dbo].[BookTypeTable] OFF
SET IDENTITY_INSERT [dbo].[DepartmentTable] ON 

INSERT [dbo].[DepartmentTable] ([DepartmentID], [Name], [UserID]) VALUES (1, N'Food', 1)
SET IDENTITY_INSERT [dbo].[DepartmentTable] OFF
SET IDENTITY_INSERT [dbo].[DesignationTable] ON 

INSERT [dbo].[DesignationTable] ([DesignationID], [Name], [UserID], [Scale]) VALUES (1, N'Operator', 1, N'16')
SET IDENTITY_INSERT [dbo].[DesignationTable] OFF
SET IDENTITY_INSERT [dbo].[EmployeeTable] ON 

INSERT [dbo].[EmployeeTable] ([EmployeeID], [UserID], [FullName], [FatherName], [ContactNo], [Email], [Address], [DesignationID], [DepartmentID], [IsActive], [Description]) VALUES (1, 1, N'Asad Khan', N'Salman Khan', N'03146565655', N'asadkhan@gmail.com', N'Mardan Charsadda', 1, 1, 1, N'ACBC')
INSERT [dbo].[EmployeeTable] ([EmployeeID], [UserID], [FullName], [FatherName], [ContactNo], [Email], [Address], [DesignationID], [DepartmentID], [IsActive], [Description]) VALUES (2, 1, N'Junaid', N'Hamza', N'03459696968', N'junaid@gmail.com', N'Mardan Charsadda', 1, 1, 1, N'You Welcome')
SET IDENTITY_INSERT [dbo].[EmployeeTable] OFF
SET IDENTITY_INSERT [dbo].[IssueBookTable] ON 

INSERT [dbo].[IssueBookTable] ([IssueBookID], [UserID], [BookID], [EmployeeID], [IssueCopies], [IssueDate], [ReturnDate], [Status], [Description], [ReserveNoOfCopies]) VALUES (1, 1, 1, 1, 2, CAST(N'2019-12-10' AS Date), CAST(N'2019-12-15' AS Date), 0, N'You Welcome', 0)
INSERT [dbo].[IssueBookTable] ([IssueBookID], [UserID], [BookID], [EmployeeID], [IssueCopies], [IssueDate], [ReturnDate], [Status], [Description], [ReserveNoOfCopies]) VALUES (2, 1, 2, 1, 2, CAST(N'2019-12-20' AS Date), CAST(N'2019-12-22' AS Date), 0, N'You Welcome', 0)
INSERT [dbo].[IssueBookTable] ([IssueBookID], [UserID], [BookID], [EmployeeID], [IssueCopies], [IssueDate], [ReturnDate], [Status], [Description], [ReserveNoOfCopies]) VALUES (11, 2, 2, 2, 1, CAST(N'2019-12-20' AS Date), CAST(N'2019-12-22' AS Date), 1, N'Approve', 0)
INSERT [dbo].[IssueBookTable] ([IssueBookID], [UserID], [BookID], [EmployeeID], [IssueCopies], [IssueDate], [ReturnDate], [Status], [Description], [ReserveNoOfCopies]) VALUES (12, 2, 1, 2, 1, CAST(N'2019-12-20' AS Date), CAST(N'2019-12-22' AS Date), 0, N'Reserve Request!', 1)
SET IDENTITY_INSERT [dbo].[IssueBookTable] OFF
SET IDENTITY_INSERT [dbo].[PurchaseDetailTable] ON 

INSERT [dbo].[PurchaseDetailTable] ([PurchaseDetailID], [BookID], [PurchaseID], [Qty], [UnitPrice]) VALUES (1, 1, 1, 4, 5000)
INSERT [dbo].[PurchaseDetailTable] ([PurchaseDetailID], [BookID], [PurchaseID], [Qty], [UnitPrice]) VALUES (2, 2, 1, 2, 4500)
INSERT [dbo].[PurchaseDetailTable] ([PurchaseDetailID], [BookID], [PurchaseID], [Qty], [UnitPrice]) VALUES (3, 1, 2, 4, 5000)
INSERT [dbo].[PurchaseDetailTable] ([PurchaseDetailID], [BookID], [PurchaseID], [Qty], [UnitPrice]) VALUES (4, 2, 2, 2, 4500)
SET IDENTITY_INSERT [dbo].[PurchaseDetailTable] OFF
SET IDENTITY_INSERT [dbo].[PurchaseTable] ON 

INSERT [dbo].[PurchaseTable] ([PurchaseID], [PurchaseDate], [UserID], [PurchaseAmount], [SupplierID]) VALUES (1, CAST(N'2019-12-20' AS Date), 1, 29000, 1)
INSERT [dbo].[PurchaseTable] ([PurchaseID], [PurchaseDate], [UserID], [PurchaseAmount], [SupplierID]) VALUES (2, CAST(N'2019-12-20' AS Date), 1, 29000, 2)
SET IDENTITY_INSERT [dbo].[PurchaseTable] OFF
SET IDENTITY_INSERT [dbo].[SupplierTable] ON 

INSERT [dbo].[SupplierTable] ([SupplierID], [SupplierName], [UserID], [ContactNo], [Email], [Description]) VALUES (1, N'Shaheen Book Dealer ', 1, N'03143076781', N'shaheen@gmail.com', N'ABC')
INSERT [dbo].[SupplierTable] ([SupplierID], [SupplierName], [UserID], [ContactNo], [Email], [Description]) VALUES (2, N'SAB Book Dealer ', 1, N'03152525255', N'js@gmail.com', N'ABC')
SET IDENTITY_INSERT [dbo].[SupplierTable] OFF
SET IDENTITY_INSERT [dbo].[UserTable] ON 

INSERT [dbo].[UserTable] ([UserID], [UserTypeID], [UserName], [Password], [EmployeeID], [IsActive]) VALUES (1, 1, N'Admin', N'admin', 1, 1)
INSERT [dbo].[UserTable] ([UserID], [UserTypeID], [UserName], [Password], [EmployeeID], [IsActive]) VALUES (2, 2, N'Junaid', N'12345', 2, 1)
SET IDENTITY_INSERT [dbo].[UserTable] OFF
SET IDENTITY_INSERT [dbo].[UserTypeTable] ON 

INSERT [dbo].[UserTypeTable] ([UserTypeID], [UserType]) VALUES (1, N'Admin')
INSERT [dbo].[UserTypeTable] ([UserTypeID], [UserType]) VALUES (2, N'Employee')
SET IDENTITY_INSERT [dbo].[UserTypeTable] OFF
ALTER TABLE [dbo].[BookFineTable]  WITH CHECK ADD  CONSTRAINT [FK_BookFineTable_BookTable] FOREIGN KEY([BookID])
REFERENCES [dbo].[BookTable] ([BookID])
GO
ALTER TABLE [dbo].[BookFineTable] CHECK CONSTRAINT [FK_BookFineTable_BookTable]
GO
ALTER TABLE [dbo].[BookFineTable]  WITH CHECK ADD  CONSTRAINT [FK_BookFineTable_EmployeeTable] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EmployeeTable] ([EmployeeID])
GO
ALTER TABLE [dbo].[BookFineTable] CHECK CONSTRAINT [FK_BookFineTable_EmployeeTable]
GO
ALTER TABLE [dbo].[BookFineTable]  WITH CHECK ADD  CONSTRAINT [FK_BookFineTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[BookFineTable] CHECK CONSTRAINT [FK_BookFineTable_UserTable]
GO
ALTER TABLE [dbo].[BookReturnTable]  WITH CHECK ADD  CONSTRAINT [FK_BookReturnTable_BookTable] FOREIGN KEY([BookID])
REFERENCES [dbo].[BookTable] ([BookID])
GO
ALTER TABLE [dbo].[BookReturnTable] CHECK CONSTRAINT [FK_BookReturnTable_BookTable]
GO
ALTER TABLE [dbo].[BookReturnTable]  WITH CHECK ADD  CONSTRAINT [FK_BookReturnTable_EmployeeTable] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EmployeeTable] ([EmployeeID])
GO
ALTER TABLE [dbo].[BookReturnTable] CHECK CONSTRAINT [FK_BookReturnTable_EmployeeTable]
GO
ALTER TABLE [dbo].[BookReturnTable]  WITH CHECK ADD  CONSTRAINT [FK_BookReturnTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[BookReturnTable] CHECK CONSTRAINT [FK_BookReturnTable_UserTable]
GO
ALTER TABLE [dbo].[BookTable]  WITH CHECK ADD  CONSTRAINT [FK_BookTable_BookTypeTable] FOREIGN KEY([BookTypeID])
REFERENCES [dbo].[BookTypeTable] ([BookTypeID])
GO
ALTER TABLE [dbo].[BookTable] CHECK CONSTRAINT [FK_BookTable_BookTypeTable]
GO
ALTER TABLE [dbo].[BookTable]  WITH CHECK ADD  CONSTRAINT [FK_BookTable_DepartmentTable] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[DepartmentTable] ([DepartmentID])
GO
ALTER TABLE [dbo].[BookTable] CHECK CONSTRAINT [FK_BookTable_DepartmentTable]
GO
ALTER TABLE [dbo].[BookTable]  WITH CHECK ADD  CONSTRAINT [FK_BookTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[BookTable] CHECK CONSTRAINT [FK_BookTable_UserTable]
GO
ALTER TABLE [dbo].[DepartmentTable]  WITH CHECK ADD  CONSTRAINT [FK_DepartmentTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[DepartmentTable] CHECK CONSTRAINT [FK_DepartmentTable_UserTable]
GO
ALTER TABLE [dbo].[DesignationTable]  WITH CHECK ADD  CONSTRAINT [FK_DesignationTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[DesignationTable] CHECK CONSTRAINT [FK_DesignationTable_UserTable]
GO
ALTER TABLE [dbo].[EmployeeTable]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeTable_DepartmentTable] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[DepartmentTable] ([DepartmentID])
GO
ALTER TABLE [dbo].[EmployeeTable] CHECK CONSTRAINT [FK_EmployeeTable_DepartmentTable]
GO
ALTER TABLE [dbo].[EmployeeTable]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeTable_DesignationTable] FOREIGN KEY([DesignationID])
REFERENCES [dbo].[DesignationTable] ([DesignationID])
GO
ALTER TABLE [dbo].[EmployeeTable] CHECK CONSTRAINT [FK_EmployeeTable_DesignationTable]
GO
ALTER TABLE [dbo].[IssueBookTable]  WITH CHECK ADD  CONSTRAINT [FK_IssueBookTable_BookTable] FOREIGN KEY([BookID])
REFERENCES [dbo].[BookTable] ([BookID])
GO
ALTER TABLE [dbo].[IssueBookTable] CHECK CONSTRAINT [FK_IssueBookTable_BookTable]
GO
ALTER TABLE [dbo].[IssueBookTable]  WITH CHECK ADD  CONSTRAINT [FK_IssueBookTable_EmployeeTable] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EmployeeTable] ([EmployeeID])
GO
ALTER TABLE [dbo].[IssueBookTable] CHECK CONSTRAINT [FK_IssueBookTable_EmployeeTable]
GO
ALTER TABLE [dbo].[IssueBookTable]  WITH CHECK ADD  CONSTRAINT [FK_IssueBookTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[IssueBookTable] CHECK CONSTRAINT [FK_IssueBookTable_UserTable]
GO
ALTER TABLE [dbo].[PurchaseDetailTable]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetailTable_BookTable] FOREIGN KEY([BookID])
REFERENCES [dbo].[BookTable] ([BookID])
GO
ALTER TABLE [dbo].[PurchaseDetailTable] CHECK CONSTRAINT [FK_PurchaseDetailTable_BookTable]
GO
ALTER TABLE [dbo].[PurchaseDetailTable]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetailTable_PurchaseTable] FOREIGN KEY([PurchaseID])
REFERENCES [dbo].[PurchaseTable] ([PurchaseID])
GO
ALTER TABLE [dbo].[PurchaseDetailTable] CHECK CONSTRAINT [FK_PurchaseDetailTable_PurchaseTable]
GO
ALTER TABLE [dbo].[PurchaseTable]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseTable_SupplierTable] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[SupplierTable] ([SupplierID])
GO
ALTER TABLE [dbo].[PurchaseTable] CHECK CONSTRAINT [FK_PurchaseTable_SupplierTable]
GO
ALTER TABLE [dbo].[PurchaseTable]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[PurchaseTable] CHECK CONSTRAINT [FK_PurchaseTable_UserTable]
GO
ALTER TABLE [dbo].[PurTemDetailsTable]  WITH CHECK ADD  CONSTRAINT [FK_PurTemDetailsTable_BookTable] FOREIGN KEY([BookID])
REFERENCES [dbo].[BookTable] ([BookID])
GO
ALTER TABLE [dbo].[PurTemDetailsTable] CHECK CONSTRAINT [FK_PurTemDetailsTable_BookTable]
GO
ALTER TABLE [dbo].[SupplierTable]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTable_UserTable] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTable] ([UserID])
GO
ALTER TABLE [dbo].[SupplierTable] CHECK CONSTRAINT [FK_SupplierTable_UserTable]
GO
ALTER TABLE [dbo].[UserTable]  WITH CHECK ADD  CONSTRAINT [FK_UserTable_EmployeeTable] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[EmployeeTable] ([EmployeeID])
GO
ALTER TABLE [dbo].[UserTable] CHECK CONSTRAINT [FK_UserTable_EmployeeTable]
GO
ALTER TABLE [dbo].[UserTable]  WITH CHECK ADD  CONSTRAINT [FK_UserTable_UserTypeTable] FOREIGN KEY([UserTypeID])
REFERENCES [dbo].[UserTypeTable] ([UserTypeID])
GO
ALTER TABLE [dbo].[UserTable] CHECK CONSTRAINT [FK_UserTable_UserTypeTable]
GO
USE [master]
GO
ALTER DATABASE [OnlineLibraryMgtSystemDb] SET  READ_WRITE 
GO
